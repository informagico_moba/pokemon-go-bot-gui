﻿using AllEnum;
using PokemonGo.RocketAPI;
using PokemonGo.RocketAPI.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace Pokémon_GO_Bot_GUI
{
    public class Settings : ISettings
    {
        public string TransferType
        {
            get
            {
                return GetSetting() != string.Empty ? GetSetting() : "none";
            }
            set
            {
                SetSetting(value.ToString());
            }
        }
        public int TransferCPThreshold
        {
            get
            {
                return GetSetting() != string.Empty ? int.Parse(GetSetting(), CultureInfo.InvariantCulture) : 0;
            }
            set
            {
                SetSetting(value.ToString());
            }
        }

        public bool EvolveAllGivenPokemons
        {
            get
            {
                return GetSetting() != string.Empty ? System.Convert.ToBoolean(GetSetting(), CultureInfo.InvariantCulture) : false;
            }
            set
            {
                SetSetting(value.ToString());
            }
        }


        public AuthType AuthType
        {
            get
            {
                return (GetSetting() != string.Empty ? GetSetting() : "Ptc") == "Ptc" ? AuthType.Ptc : AuthType.Google;
            }
            set
            {
                SetSetting(value.ToString());
            }
        }
        public string PtcUsername
        {
            get
            {
                return GetSetting() != string.Empty ? GetSetting() : "username";
            }
            set
            {
                SetSetting(value != string.Empty ? value : "username");
            }
        }
        public string PtcPassword
        {
            get
            {
                return GetSetting() != string.Empty ? GetSetting() : "password";
            }
            set
            {
                SetSetting(value != string.Empty ? value : "username");
            }
        }

        public double DefaultLatitude
        {
            get
            {
                return GetSetting() != string.Empty ? double.Parse(GetSetting(), CultureInfo.InvariantCulture) : 51.22640;
            }
            set
            {
                SetSetting(value.ToString().Replace(",", "."));
            }
        }

        //Default Amsterdam Central Station if another location is not specified
        public double DefaultLongitude
        {
            get
            {
                return GetSetting() != string.Empty ? double.Parse(GetSetting(), CultureInfo.InvariantCulture) : 6.77874;
            }
            set
            {
                SetSetting(value.ToString().Replace(",", "."));
            }
        }

        public string GoogleRefreshToken
        {
            get { return GetSetting() != string.Empty ? GetSetting() : string.Empty; }
            set { SetSetting(value); }
        }

        public bool Recycler
        {
            get
            {
                return GetSetting() != string.Empty ? System.Convert.ToBoolean(GetSetting(), CultureInfo.InvariantCulture) : false;
            }
            set
            {
                SetSetting(value.ToString());
            }
        }

        ICollection<KeyValuePair<ItemId, int>> ISettings.ItemRecycleFilter
        {
            get
            {
                //Type and amount to keep
                return new[]
                {
                    new KeyValuePair<ItemId, int>(ItemId.ItemPokeBall, 20),
                    new KeyValuePair<ItemId, int>(ItemId.ItemGreatBall, 50),
                    new KeyValuePair<ItemId, int>(ItemId.ItemUltraBall, 100),
                    new KeyValuePair<ItemId, int>(ItemId.ItemMasterBall, 200),
                    new KeyValuePair<ItemId, int>(ItemId.ItemRazzBerry, 20),
                    new KeyValuePair<ItemId, int>(ItemId.ItemRevive, 20),
                    new KeyValuePair<ItemId, int>(ItemId.ItemPotion, 0),
                    new KeyValuePair<ItemId, int>(ItemId.ItemSuperPotion, 0),
                    new KeyValuePair<ItemId, int>(ItemId.ItemHyperPotion, 50)
                };
            }
        }

        public int RecycleItemsInterval
        {
            get
            {
                return GetSetting() != string.Empty ? Convert.ToInt16(GetSetting()) : 60;
            }
            set
            {
                SetSetting(value.ToString());
            }
        }

        public string RazzBerryMode
        {
            get
            {
                return GetSetting() != string.Empty ? GetSetting() : "cp";
            }
            set
            {
                SetSetting(value.ToString());
            }
        }

        public double RazzBerrySetting
        {
            get
            {
                return GetSetting() != string.Empty ? double.Parse(GetSetting(), CultureInfo.InvariantCulture) : 500;
            }
            set
            {
                SetSetting(value.ToString());
            }
        }

        public List<AllEnum.PokemonId> DoNotTransfer
        {
            get
            {
                return GetPokemonList();
            }
            set
            {
                SetPokemonList(value);
            }
        }

        public List<AllEnum.PokemonId> UnwantedPokemon
        {
            get
            {
                return GetPokemonList();
            }
            set
            {
                SetPokemonList(value);
            }
        }

        // SETTINGS METHODS, DO NOT TOUCH
        private string GetSetting([CallerMemberName] string key = null)
        {
            return ConfigurationManager.AppSettings[key];
        }

        private List<AllEnum.PokemonId> GetPokemonList([CallerMemberName] string key = null)
        {
            string pokemonList = ConfigurationManager.AppSettings[key];

            if (pokemonList == null || pokemonList == String.Empty)
                return null;

            List<AllEnum.PokemonId> pokemons = new List<PokemonId>();
            
            foreach (string s in pokemonList.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                pokemons.Add((AllEnum.PokemonId)Convert.ToInt32(s));

            return pokemons;
        }

        private void SetPokemonList(List<AllEnum.PokemonId> value, [CallerMemberName] string key = null)
        {
            string valueToSet = "";

            foreach (AllEnum.PokemonId pokemon in value)
                valueToSet += (int)pokemon + ",";

            if (valueToSet.Length > 0)
                valueToSet = valueToSet.Remove(valueToSet.Length - 1);

            SetSetting(valueToSet, key);
        }

        private void SetSetting(string value, [CallerMemberName] string key = null)
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (key != null) configFile.AppSettings.Settings[key].Value = value;
            configFile.AppSettings.SectionInformation.ForceSave = true;
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
