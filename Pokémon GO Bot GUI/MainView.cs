﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET;
using AllEnum;
using PokemonGo.RocketAPI.Enums;
using PokemonGo.RocketAPI.Extensions;
using PokemonGo.RocketAPI.GeneratedCode;
using PokemonGo.RocketAPI;
using System.Runtime.InteropServices;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Text;

namespace Pokémon_GO_Bot_GUI
{
    public partial class MainView : Form
    {
        #region MAKE FORM MOVABLE
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void MainView_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        #endregion

        #region FUCK YOU CONSOLE.WRITELINE

        public class MyLog : TextWriter
        {
            private Control textbox;

            public MyLog(Control textbox)
            {
                this.textbox = textbox;
            }

            public override void Write(string value)
            {
                textbox.Invoke((Action)delegate { (textbox as TextBox).AppendText(value.ToString()); });
            }

            public override void WriteLine(string value)
            {
                // workaround -- shitty but works
                if (value.StartsWith("Put RefreshToken in settings for direct login"))
                {
                    string token = value.Substring(value.IndexOf(":") + 1).Trim();
                    ClientSettings.GoogleRefreshToken = token;
                    value = "Google Token saved in your configuration: " + token;
                }
                this.Write(value + "\r\n");
            }

            public override Encoding Encoding
            {
                get { return Encoding.ASCII; }
            }
        }

        #endregion

        // define a clientsettings object
        public static ISettings ClientSettings = new Settings();

        // define a cancellationtoken
        CancellationTokenSource cts;

        // define an enumerator for found objects
        private enum ObjectType
        {
            tPokestop,
            tPokemon,
            tGym,
            tPlayer
        }

        // define a class for the markers to be shown
        private class Marker
        {
            public double Lat = 0.0;
            public double Lon = 0.0;
            public string Text = "";
            public ObjectType Type;
            public Bitmap Image;

            public Marker(double _lat, double _lon, string _text, Bitmap _image, ObjectType _type)
            {
                this.Lon = _lon;
                this.Lat = _lat;
                this.Text = _text;
                this.Image = _image;
                this.Type = _type;
            }

            public Marker() { }
        }

        // define list of markers currently displayed
        static List<Marker> markerToDisplay = new List<Marker>();
        static Marker playerToDisplay = new Marker();

        public MainView()
        {
            InitializeComponent();
        }

        #region CONTROLBOX
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMaximize_Click(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;
        }

        private void btnReduce_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        #endregion

        #region MAP METHODS
        private void setMapLocation()
        {
            pokeMap.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
            pokeMap.Position = new PointLatLng(ClientSettings.DefaultLatitude, ClientSettings.DefaultLongitude);
            pokeMap.Zoom = 14;

            // update label position
            updateLabelPosition(pokeMap.Position);
        }

        private void setMapOverlays()
        {
            GMapOverlay overlay = new GMapOverlay("pokestops");
            pokeMap.Overlays.Add(overlay);

            overlay = new GMapOverlay("pokemons");
            pokeMap.Overlays.Add(overlay);

            overlay = new GMapOverlay("gyms");
            pokeMap.Overlays.Add(overlay);

            overlay = new GMapOverlay("player");
            pokeMap.Overlays.Add(overlay);
        }

        private List<Marker> getMarkerAtPosition(double lat, double lon)
        {
            // barely thread safe
            List<Marker> listMarker = markerToDisplay;

            return (from q in listMarker where q.Lat == lat && q.Lon == lon select q).ToList();
        }

        private void removeMarkerAtPosition(double lat, double lon, ObjectType type)
        {
            // remove marker from the list
            if (type != ObjectType.tPlayer)
            {
                foreach (Marker m in getMarkerAtPosition(lat, lon).Where(x => x.Type == type))
                    markerToDisplay.Remove(m);
            }
            else
            {
                playerToDisplay = new Marker();
            }

            // remove it also from the map
            switch (type)
            {
                case ObjectType.tGym:
                    GMapOverlay markersGyms = new GMapOverlay("gyms");
                    foreach (GMarkerGoogle m in markersGyms.Markers.Select(x => x).Where(y => y.Position.Lat == lat && y.Position.Lng == lon).ToList())
                        markersGyms.Markers.Remove(m);
                    break;
                case ObjectType.tPlayer:
                    // for the payer remove all current markers
                    // only one must be shown
                    GMapOverlay markersPlayer = new GMapOverlay("player");
                    foreach (GMarkerGoogle m in markersPlayer.Markers) //.Select(x => x).Where(y => y.Position.Lat == lat && y.Position.Lng == lon).ToList())
                        markersPlayer.Markers.Remove(m);
                    break;
                case ObjectType.tPokemon:
                    GMapOverlay markersPokemons = new GMapOverlay("pokemons");
                    foreach (GMarkerGoogle m in markersPokemons.Markers.Select(x => x).Where(y => y.Position.Lat == lat && y.Position.Lng == lon).ToList())
                        markersPokemons.Markers.Remove(m);
                    break;
                case ObjectType.tPokestop:
                    GMapOverlay markersPokestops = new GMapOverlay("pokestops");
                    foreach (GMarkerGoogle m in markersPokestops.Markers.Select(x => x).Where(y => y.Position.Lat == lat && y.Position.Lng == lon).ToList())
                        markersPokestops.Markers.Remove(m);
                    break;
            }
        }

        private void addNewMarkerToMap(double lat, double lon, Bitmap image, string text, int index)
        {
            GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(lat, lon), image);
            GMapOverlay markersOverlay = pokeMap.Overlays[index];
            marker.ToolTipText = text;
            markersOverlay.Markers.Add(marker);
            pokeMap.Invoke((Action)delegate { pokeMap.Refresh(); });
        }

        private void setMapPokestop(double lat, double lon, FortLureInfo lured, FortSponsor sponsor)
        {
            string text = "";
            Bitmap image;

            // remove already existing marker in that position
            removeMarkerAtPosition(lat, lon, ObjectType.tPokestop);

            if (lured != null)
            {
                image = Properties.Resources.PstopLured;
                text += "Lured: YES\r\n";
                text += "Active Pokemon: " + lured.ActivePokemonId.ToString() + "\r\n";
            }
            else
            {
                image = Properties.Resources.Pstop;
                text += "Lured: NO\r\n";
            }

            // resize image
            image = new Bitmap(image, new Size(18, 18));

            if (sponsor != FortSponsor.UnsetSponsor)
                text += "Sponsor: " + sponsor.ToString();

            // add marker to the list
            markerToDisplay.Add(new Marker(lat, lon, text.Trim(), image, ObjectType.tPokestop));

            // display marker in the map
            addNewMarkerToMap(lat, lon, image, text, (int)ObjectType.tPokestop);
        }

        private void setMapPokemon(double lat, double lon, PokemonId id, int cp, CatchPokemonResponse.Types.CatchStatus caughtStatus)
        {
            string text = "";
            Bitmap image = getPokemonImage(id);
            //image.Height

            // remove already existing marker in that position
            removeMarkerAtPosition(lat, lon, ObjectType.tPokemon);

            text += "Pokèmon: " + id.ToString() + "\r\n";
            text += "CP: " + cp + "\r\n";
            text += "Caught: ";
            switch (caughtStatus)
            {
                case CatchPokemonResponse.Types.CatchStatus.CatchSuccess:
                    text += "YES";
                    break;
                default:
                    text += "NO";
                    break;
            }

            // resize image
            image = new Bitmap(image, new Size(18, 18));

            // add marker to the list
            markerToDisplay.Add(new Marker(lat, lon, text, image, ObjectType.tPokemon));

            // display marker in the map
            addNewMarkerToMap(lat, lon, image, text, (int)ObjectType.tPokemon);
        }

        #endregion

        #region MAP EVENTS
        private void pokeMap_OnMarkerClick(GMapMarker item, MouseEventArgs e)
        {

        }

        private void pokeMap_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                PointLatLng position = new PointLatLng(pokeMap.FromLocalToLatLng(e.X, e.Y).Lat, pokeMap.FromLocalToLatLng(e.X, e.Y).Lng);
                pokeMap.Position = position;

                if (MessageBox.Show("Set this as the current BOT location?\r\nLAT: " + position.Lat + "\r\nLON: " + position.Lng, "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ClientSettings.DefaultLatitude = position.Lat;
                    ClientSettings.DefaultLongitude = position.Lng;

                    updateLabelPosition(position);
                }
            }
        }
        #endregion

        #region MENU EVENTS
        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // reset status
            setStatus("", false);

            // run login task
            Task.Run(() => Login());
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // run bot task
            if (cts == null)
            {
                cts = new CancellationTokenSource();

                Task.Run(() => Execute());

                startToolStripMenuItem.Text = "Stop";
            }
            else
            {
                cts.Cancel();
                cts = null;

                startToolStripMenuItem.Text = "Start";
                setStatus("Waiting for BOT to stop...", false);

                enableBotStart(false);
            }
        }

        private void optionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // open settings
            SettingsView settingsView = new SettingsView();
            settingsView.ShowDialog();

            // reload map
            setMapLocation();
        }

        private void creditsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Project repository:\r\nhttps://bitbucket.org/bwrkr/pokemon-go-bot-gui.git\r\n\r\nCredits:\r\nMany thanks to Ferox + Neer + nonm for the API.\r\nThanks also to AHAAAAAAA devs for the assets.\r\n\r\nhttps://github.com/DetectiveSquirrel/Pokemon-Go-Rocket-API\r\nhttps://github.com/AHAAAAAAA/PokemonGo-Map.git", "Thanks!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void openRepositoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://bitbucket.org/bwrkr/pokemon-go-bot-gui.git");
        }
        #endregion

        #region FORM EVENTS
        private void Form1_Load(object sender, EventArgs e)
        {
            // redirect console output to the txtLog control
            Console.SetOut(new MyLog(txtLog));
        }

        private void MainView_Shown(object sender, EventArgs e)
        {
            // setup map
            setMapLocation();

            // setup overlays
            setMapOverlays();

            // disclaimer
            MessageBox.Show("*** WARNING! ***\r\n\r\nI don't take any responsibility for the incorrect use of this BOT.\r\nI you make big 'jumps' there's a high risk of being banned (temporarily / permanently), and this is not related to the bot.\r\n\r\nPlease, change Edit>Settings before start the BOT.", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        #endregion

        #region FORM METHODS

        private void enableBotStart(bool tf)
        {
            menuMain.Invoke((Action)delegate { startToolStripMenuItem.Enabled = tf; });
        }

        private void enableSelectors(bool tf)
        {
            btnProfile.Invoke((Action)delegate { btnProfile.Enabled = tf; });
            btnShowPokemon.Invoke((Action)delegate { btnShowPokemon.Enabled = tf; });
            btnShowItems.Invoke((Action)delegate { btnShowItems.Enabled = tf; });
        }

        private void showLoginErrorMessage()
        {
            setStatus("Unable to perform login!", false);

            panelBottom.Invoke((Action)delegate { panelBottom.BackColor = Color.DimGray; });
            Thread.Sleep(500);
            panelBottom.Invoke((Action)delegate { panelBottom.BackColor = Color.FromArgb(255, 255, 128, 128); });
            Thread.Sleep(500);
            panelBottom.Invoke((Action)delegate { panelBottom.BackColor = Color.DimGray; });
            Thread.Sleep(500);
            panelBottom.Invoke((Action)delegate { panelBottom.BackColor = Color.FromArgb(255, 255, 128, 128); });
            Thread.Sleep(500);
        }

        private void setStatus(string value, bool append)
        {
            lblStatus.Invoke((Action)delegate
            {
                if (append)
                    lblStatus.Text += value;
                else
                    lblStatus.Text = value;
            });
        }

        private void updateLabelPosition(PointLatLng position)
        {
            lblPosition.Invoke((Action)delegate { lblPosition.Text = "(BOT Position) Lat: " + position.Lat.ToString() + " - Lon: " + position.Lng.ToString(); });
        }
        #endregion

        #region RESOURCES METHODS
        static Bitmap getPokemonImage(PokemonId id)
        {
            return (Bitmap)Properties.Resources.ResourceManager.GetObject("_" + ((int)id).ToString(), Properties.Resources.Culture);
        }
        #endregion

        #region DATAGRIDVIEW METHODS
        private void setDGVColumns(int _datatype)
        {
            switch (_datatype)
            {
                case 0: // profile

                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Clear(); });

                    DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "Key";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "Value";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });
                    break;
                case 1: // pokemon
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Clear(); });

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "#";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });

                    DataGridViewImageColumn columnI = new DataGridViewImageColumn();
                    columnI.HeaderText = "Image";
                    columnI.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(columnI); });

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "Pokemon";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "CP";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "Height (m)";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "Weight (Kg)";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "Nickname";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "Stamina";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "Stamina Max";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });
                    break;
                case 2: // profile

                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Clear(); });

                    column = new DataGridViewTextBoxColumn();

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "Item";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });

                    column = new DataGridViewTextBoxColumn();
                    column.HeaderText = "Count";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    displayDGV.Invoke((Action)delegate { displayDGV.Columns.Add(column); });
                    break;
            }
        }
        #endregion

        #region BOTMETHODS

        // define client globally
        Client client;

        private async Task Login()
        {
            // define error return
            bool error = false;

            // set status
            setStatus("Logging in...", false);

            // disable selectors
            enableSelectors(false);

            // disable bot start
            enableBotStart(false);

            retryGoogleLogin:

            try
            {
                // generate new client
                client = new Client(ClientSettings);

                // do login
                if (ClientSettings.AuthType == AuthType.Ptc)
                    await client.DoPtcLogin(ClientSettings.PtcUsername, ClientSettings.PtcPassword);
                else if (ClientSettings.AuthType == AuthType.Google)
                    await client.DoGoogleLogin();

                // set server
                await client.SetServer();

                // enable selectors
                enableSelectors(true);

                // enable bot start
                enableBotStart(true);

                // set status
                setStatus("Logged in successfully!", false);

                // set error false
                error = false;
            }
            catch
            {
                error = true;
            }

            if (error)
            {
                if (ClientSettings.AuthType == AuthType.Google && ClientSettings.GoogleRefreshToken != "")
                {
                    ClientSettings.GoogleRefreshToken = "";
                    goto retryGoogleLogin;
                }
                else
                    showLoginErrorMessage();
            }
        }

        // execute, main task
        private async void Execute()
        {
            // check if cancellation is requested
            if (cts == null || cts.IsCancellationRequested)
                goto stopBOT;
            
            // do login
            await Login();

            // set status
            setStatus("Logged in successfully! > BOT RUNNING...", false);

            // get profile
            var profile = await client.GetProfile();

            // get settings
            var settings = await client.GetSettings();

            // get map objects
            var mapObjects = await client.GetMapObjects();

            // get inventory
            var inventory = await client.GetInventory();
            // get pokemons in the inventory
            var pokemons = inventory.InventoryDelta.InventoryItems.Select(i => i.InventoryItemData?.Pokemon)
                           .Where(p => p != null && p?.PokemonId > 0);

            // check if cancellation is requested
            if (cts == null || cts.IsCancellationRequested)
                goto stopBOT;

            try
            {
                Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] BOT Started...\r\n");

                // transfer pokemons (if needed)
                if (ClientSettings.TransferType == "leaveStrongest")
                    await TransferAllButStrongestUnwantedPokemon(client);
                else if (ClientSettings.TransferType == "all")
                    await TransferAllGivenPokemons(client, pokemons, true);
                else if (ClientSettings.TransferType == "duplicate")
                    await TransferDuplicatePokemon(client);
                else if (ClientSettings.TransferType == "cp")
                    await TransferAllWeakPokemon(client, ClientSettings.TransferCPThreshold);
                else
                    Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Transferring pokemon is disabled!");

                // check if cancellation is requested
                if (cts == null || cts.IsCancellationRequested)
                    goto stopBOT;

                // evolve pokemon (if needed)
                if (ClientSettings.EvolveAllGivenPokemons)
                    await EvolveAllGivenPokemons(client, pokemons);
                Console.WriteLine();

                // recycle all useless items
                if (ClientSettings.Recycler)
                    client.RecycleItems(client);

                // check if cancellation is requested
                if (cts == null || cts.IsCancellationRequested)
                    goto stopBOT;

                await Task.Delay(5000);

                // check if cancellation is requested
                if (cts == null || cts.IsCancellationRequested)
                    goto stopBOT;
                
                // execute farming
                await ExecuteFarmingPokestopsAndPokemons(client);

                // check if cancellation is requested
                if (cts == null || cts.IsCancellationRequested)
                    goto stopBOT;
                
                Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Unexpected stop? Restarting in 20 seconds!\r\n");

                // update status
                setStatus("BOT has stopped! Restarting in 20 seconds...", false);

                await Task.Delay(20000);

                // restart (if something went wrong)
                Execute();
            }
            catch (TaskCanceledException tce) { Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Task Canceled Exception - Restarting"); Execute(); }
            catch (UriFormatException ufe) { Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] System URI Format Exception - Restarting"); Execute(); }
            catch (ArgumentOutOfRangeException aore) { Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] ArgumentOutOfRangeException - Restarting"); Execute(); }
            catch (NullReferenceException nre) { Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Null Refference - Restarting"); Execute(); }

        stopBOT:
            setStatus("BOT Stopped!", false);
            enableBotStart(true);
        }
        
        // execute farming pokestop and pokemons
        private async Task ExecuteFarmingPokestopsAndPokemons(Client client)
        {
            // get new mapObjects
            var mapObjects = await client.GetMapObjects();

            // get all pokestops
            var pokeStops =
                mapObjects.MapCells.SelectMany(i => i.Forts)
                    .Where(
                        i =>
                            i.Type == FortType.Checkpoint &&
                            i.CooldownCompleteTimestampMs < DateTime.UtcNow.ToUnixTime());

            // check if cancellation is requested
            if (cts == null || cts.IsCancellationRequested) return;

            foreach (var pokeStop in pokeStops)
            {
                // check if cancellation is requested
                if (cts == null || cts.IsCancellationRequested) return;

                // get pokestop details
                setMapPokestop(pokeStop.Latitude, pokeStop.Longitude, pokeStop.LureInfo, pokeStop.Sponsor);
                var update = await client.UpdatePlayerLocation(pokeStop.Latitude, pokeStop.Longitude);
                var fortInfo = await client.GetFort(pokeStop.Id, pokeStop.Latitude, pokeStop.Longitude);
                var fortSearch = await client.SearchFort(pokeStop.Id, pokeStop.Latitude, pokeStop.Longitude);

                // console log
                Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] PokeStop XP: {fortSearch.ExperienceAwarded}, Gems: {fortSearch.GemsAwarded}, Eggs: {(fortSearch.PokemonDataEgg != null ? "1" : "0")}, Items: {GetFriendlyItemsString(fortSearch.ItemsAwarded)}");
                if (fortSearch.Result == FortSearchResponse.Types.Result.InventoryFull)
                    Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] !WARNING! --> INVENTORY FULL!");
                Console.WriteLine();

                // check if cancellation is requested
                if (cts == null || cts.IsCancellationRequested) return;

                // wait 15secs to go to next marker
                await Task.Delay(15000);

                // check if cancellation is requested
                if (cts == null || cts.IsCancellationRequested) return;

                // fetch pokemons
                await ExecuteCatchAllNearbyPokemons(client);

                // check if cancellation is requested
                if (cts == null || cts.IsCancellationRequested) return;
            }
        }

        #region TRANSFER POKEMON

        // transfer all given pokemon
        // generic method for transfer
        private static async Task TransferAllGivenPokemons(Client client, IEnumerable<PokemonData> unwantedPokemons, bool standaloneCall)
        {
            if (standaloneCall)
                Console.WriteLine($"\r\n[{DateTime.Now.ToString("HH:mm:ss")}] Fetching pokemon to transfer...");

            foreach (var pokemon in unwantedPokemons)
            {
                var transferPokemonResponse = await client.TransferPokemon(pokemon.Id);

                /*
                ReleasePokemonOutProto.Status {
	                UNSET = 0;
	                SUCCESS = 1;
	                POKEMON_DEPLOYED = 2;
	                FAILED = 3;
	                ERROR_POKEMON_IS_EGG = 4;
                }*/

                if (transferPokemonResponse.Status == 1)
                {
                    Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Transferred {pokemon.PokemonId} with {pokemon.Cp} CP!");
                }
                else
                {
                    var status = transferPokemonResponse.Status;

                    Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Failed to transfer {pokemon.PokemonId} with {pokemon.Cp} CP! ReleasePokemonOutProto.Status was { status}");
                }

                await Task.Delay(3000);
            }

            if (standaloneCall)
                Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Finished transferring pokemon!\r\n");
        }

        // transfer all but unwanted pokemon
        private static async Task TransferAllButStrongestUnwantedPokemon(Client client)
        {
            Console.WriteLine($"\r\n[{DateTime.Now.ToString("HH:mm:ss")}] Fetching pokemon to transfer...");

            var unwantedPokemonTypes = ClientSettings.UnwantedPokemon;

            /*{
                PokemonId.Pidgey,
                PokemonId.Rattata,
                PokemonId.Weedle,
                PokemonId.Zubat,
                PokemonId.Caterpie,
                PokemonId.Pidgeotto,
                PokemonId.NidoranFemale,
                PokemonId.Paras,
                PokemonId.Venonat,
                PokemonId.Psyduck,
                PokemonId.Poliwag,
                PokemonId.Slowpoke,
                PokemonId.Drowzee,
                PokemonId.Gastly,
                PokemonId.Goldeen,
                PokemonId.Staryu,
                PokemonId.Magikarp,
                PokemonId.Clefairy,
                PokemonId.Eevee,
                PokemonId.Tentacool,
                PokemonId.Dratini,
                PokemonId.Ekans,
                PokemonId.Jynx,
                PokemonId.Lickitung,
                PokemonId.Spearow,
                PokemonId.NidoranFemale,
                PokemonId.NidoranMale
            };*/

            var inventory = await client.GetInventory();
            var pokemons = inventory.InventoryDelta.InventoryItems
                .Select(i => i.InventoryItemData?.Pokemon)
                .Where(p => p != null && p?.PokemonId > 0)
                .ToArray();

            // define a total for the pokemon to transfer
            int totalPokemonToTransfer = 0;

            foreach (var unwantedPokemonType in unwantedPokemonTypes)
            {
                var pokemonOfDesiredType = pokemons.Where(p => p.PokemonId == unwantedPokemonType)
                    .OrderByDescending(p => p.Cp)
                    .ToList();

                var unwantedPokemon =
                    pokemonOfDesiredType.Skip(1) // keep the strongest one for potential battle-evolving
                        .ToList();

                if (unwantedPokemon.Count > 0)
                {
                    Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Transferring {unwantedPokemon.Count} pokemons of type {unwantedPokemonType}...");
                    await TransferAllGivenPokemons(client, unwantedPokemon, false);
                }

                // update total
                totalPokemonToTransfer += unwantedPokemon.Count;
            }

            if (totalPokemonToTransfer == 0)
                Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] No pokemon to transfer!");
            
            Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Finished transferring pokemon!\r\n");
        }

        // transfer duplicate pokemon
        private static async Task TransferDuplicatePokemon(Client client)
        {
            Console.WriteLine($"\r\n[{DateTime.Now.ToString("HH:mm:ss")}] Fetching pokemon to transfer...");

            var inventory = await client.GetInventory();
            var allpokemons =
                inventory.InventoryDelta.InventoryItems.Select(i => i.InventoryItemData?.Pokemon)
                    .Where(p => p != null && p?.PokemonId > 0);

            var dupes = allpokemons.OrderBy(x => x.Cp).Select((x, i) => new { index = i, value = x })
                .GroupBy(x => x.value.PokemonId)
                .Where(x => x.Skip(1).Any());

            int totalPokemonToTransfer = 0;

            for (var i = 0; i < dupes.Count(); i++)
            {
                for (var j = 0; j < dupes.ElementAt(i).Count() - 1; j++)
                {
                    var dubpokemon = dupes.ElementAt(i).ElementAt(j).value;
                    var transfer = await client.TransferPokemon(dubpokemon.Id);
                    Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Transferred {dubpokemon.PokemonId} with {dubpokemon.Cp} CP (Highest is {dupes.ElementAt(i).Last().value.Cp})");

                    totalPokemonToTransfer++;
                }
            }

            if (totalPokemonToTransfer == 0)
                Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] No pokemon to transfer!");

            Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Finished transferring pokemon!\r\n");
        }

        // transfer all weak pokemon
        private static async Task TransferAllWeakPokemon(Client client, int cpThreshold)
        {
            Console.WriteLine($"\r\n[{DateTime.Now.ToString("HH:mm:ss")}] Fetching pokemon to transfer...");

            // pokemon whitelist NOT TO BE TRANSFERRED
            var doNotTransfer = ClientSettings.DoNotTransfer;


            /*new[] //these will not be transferred even when below the CP threshold
            {
                PokemonId.Pidgey,
                PokemonId.Rattata,
                PokemonId.Weedle,
                PokemonId.Zubat,
                PokemonId.Caterpie,
                PokemonId.Pidgeotto,
                PokemonId.NidoranFemale,
                PokemonId.Paras,
                PokemonId.Venonat,
                PokemonId.Psyduck,
                PokemonId.Poliwag,
                PokemonId.Slowpoke,
                PokemonId.Drowzee,
                PokemonId.Gastly,
                PokemonId.Goldeen,
                PokemonId.Staryu,
                PokemonId.Magikarp,
                PokemonId.Eevee,
                PokemonId.Dratini
            };*/

            var inventory = await client.GetInventory();
            var pokemons = inventory.InventoryDelta.InventoryItems
                                .Select(i => i.InventoryItemData?.Pokemon)
                                .Where(p => p != null && p?.PokemonId > 0)
                                .ToArray();

            // define a total for the pokemon to transfer
            int totalPokemonToTransfer = 0;

            //foreach (var unwantedPokemonType in unwantedPokemonTypes)
            {
                var pokemonToDiscard = pokemons.Where(p => !doNotTransfer.Contains(p.PokemonId) && p.Cp < cpThreshold)
                                                   .OrderByDescending(p => p.Cp)
                                                   .ToList();

                //var unwantedPokemon = pokemonOfDesiredType.Skip(1) // keep the strongest one for potential battle-evolving
                //                                          .ToList();

                if (pokemonToDiscard.Count > 0)
                {
                    Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Transferring {pokemonToDiscard.Count} pokemons below {cpThreshold}. CP..");
                    await TransferAllGivenPokemons(client, pokemonToDiscard, false);
                }

                // update total
                totalPokemonToTransfer += pokemonToDiscard.Count;
            }

            if (totalPokemonToTransfer == 0)
                Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] No pokemon to transfer!");

            Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Finished transferring pokemon!\r\n");
        }

        #endregion

        // evolve all given pokemon
        private static async Task EvolveAllGivenPokemons(Client client, IEnumerable<PokemonData> pokemonToEvolve)
        {
            Console.WriteLine($"\r\n[{DateTime.Now.ToString("HH:mm:ss")}] Fetching pokemon to evolve...");

            foreach (var pokemon in pokemonToEvolve)
            {
                /*
                enum Holoholo.Rpc.Types.EvolvePokemonOutProto.Result {
	                UNSET = 0;
	                SUCCESS = 1;
	                FAILED_POKEMON_MISSING = 2;
	                FAILED_INSUFFICIENT_RESOURCES = 3;
	                FAILED_POKEMON_CANNOT_EVOLVE = 4;
	                FAILED_POKEMON_IS_DEPLOYED = 5;
                }
                }*/

                //var countOfEvolvedUnits = 0;
                //var xpCount = 0;

                EvolvePokemonOut evolvePokemonOutProto;
                do
                {
                    Console.Write($"[{DateTime.Now.ToString("HH:mm:ss")}] Evolving {pokemon.PokemonId}...");
                    evolvePokemonOutProto = await client.EvolvePokemon(pokemon.Id);
                    //todo: someone check whether this still works

                    if (evolvePokemonOutProto.Result == 1)
                    {
                        Console.WriteLine($"successfull for {evolvePokemonOutProto.ExpAwarded} xp!");

                        //countOfEvolvedUnits++;
                        //xpCount += evolvePokemonOutProto.ExpAwarded;
                    }
                    else
                    {
                        switch (evolvePokemonOutProto.Result)
                        {
                            case 2:
                                Console.WriteLine("failed due to pokemon missing!");
                                break;
                            case 3:
                                Console.WriteLine("failed due to insufficient resources!");
                                break;
                            case 4:
                                Console.WriteLine("failed because pokemon can't evolve!");
                                break;
                            case 5:
                                Console.WriteLine("failed because pokemon is deployed!");
                                break;
                        }
                    }
                } while (evolvePokemonOutProto.Result == 1);

                //if (countOfEvolvedUnits > 0)
                //    Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Evolved {countOfEvolvedUnits} pieces of {pokemon.PokemonId} for {xpCount} xp!");

                await Task.Delay(3000);
            }

            Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Finished evolving pokemon!\r\n");
        }
        
        // get firendly items string
        private static string GetFriendlyItemsString(IEnumerable<FortSearchResponse.Types.ItemAward> items)
        {
            var enumerable = items as IList<FortSearchResponse.Types.ItemAward> ?? items.ToList();

            if (!enumerable.Any())
                return string.Empty;

            return
                enumerable.GroupBy(i => i.ItemId)
                    .Select(kvp => new { ItemName = kvp.Key.ToString(), Amount = kvp.Sum(x => x.ItemCount) })
                    .Select(y => $"{y.Amount} x {y.ItemName}")
                    .Aggregate((a, b) => $"{a}, {b}");
        }

        // execute catch all newarby pokemons
        private async Task ExecuteCatchAllNearbyPokemons(Client client)
        {
            // get new mapObjects
            var mapObjects = await client.GetMapObjects();
          
            // get pokemons
            var pokemons = mapObjects.MapCells.SelectMany(i => i.CatchablePokemons);

            // get inventory
            var inventory2 = await client.GetInventory();

            // get pokemon in the inventory
            var pokemons2 = inventory2.InventoryDelta.InventoryItems
                .Select(i => i.InventoryItemData?.Pokemon)
                .Where(p => p != null && p?.PokemonId > 0)
                .ToArray();
            
            // cycle for each pokemon
            foreach (var pokemon in pokemons)
            {
                // get pokemon details
                var update = await client.UpdatePlayerLocation(pokemon.Latitude, pokemon.Longitude);
                var encounterPokemonResponse = await client.EncounterPokemon(pokemon.EncounterId, pokemon.SpawnpointId);
                var pokemonCP = encounterPokemonResponse?.WildPokemon?.PokemonData?.Cp;

                // try to catch pokemon
                CatchPokemonResponse caughtPokemonResponse;

                do
                {
                    // use RazzBerry if needed
                    if (ClientSettings.RazzBerryMode == "cp")
                        if (pokemonCP > ClientSettings.RazzBerrySetting)
                            await client.UseRazzBerry(client, pokemon.EncounterId, pokemon.SpawnpointId);
                    if (ClientSettings.RazzBerryMode == "probability")
                        if (encounterPokemonResponse.CaptureProbability.CaptureProbability_.First() < ClientSettings.RazzBerrySetting)
                            await client.UseRazzBerry(client, pokemon.EncounterId, pokemon.SpawnpointId);

                    caughtPokemonResponse = await client.CatchPokemon(pokemon.EncounterId, pokemon.SpawnpointId, pokemon.Latitude, pokemon.Longitude, MiscEnums.Item.ITEM_POKE_BALL, pokemonCP);
                } while (caughtPokemonResponse.Status == CatchPokemonResponse.Types.CatchStatus.CatchMissed);

                if (caughtPokemonResponse.Status == CatchPokemonResponse.Types.CatchStatus.CatchSuccess)
                    Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Caught a {pokemon.PokemonId} with {encounterPokemonResponse?.WildPokemon?.PokemonData?.Cp} CP");
                else
                    Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] {pokemon.PokemonId} with {encounterPokemonResponse?.WildPokemon?.PokemonData?.Cp} CP got away..");

                // decide what to do with the pokemon
                if (ClientSettings.TransferType == "leaveStrongest")
                    await TransferAllButStrongestUnwantedPokemon(client);
                else if (ClientSettings.TransferType == "all")
                    await TransferAllGivenPokemons(client, pokemons2, true);
                else if (ClientSettings.TransferType == "duplicate")
                    await TransferDuplicatePokemon(client);
                else if (ClientSettings.TransferType == "cp")
                    await TransferAllWeakPokemon(client, ClientSettings.TransferCPThreshold);

                setMapPokemon(pokemon.Latitude, pokemon.Longitude, pokemon.PokemonId, (int)pokemonCP, caughtPokemonResponse.Status);

                // delay the task
                await Task.Delay(3000);
            }
        }

        #endregion

        #region SELECTOR METHODS

        private async void ShowProfile()
        {
            // setup datagridview
            setDGVColumns(0);

            // get profile
            var profile = await client.GetProfile();

            displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add("Username", profile.Profile.Username); });
            displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add("Team", profile.Profile.Team.ToString()); });
            displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add("Pokecoin", profile.Profile.Currency[0].Amount.ToString()); });
            displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add("Stardust", profile.Profile.Currency[1].Amount.ToString()); });
            displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add("Item Storage", profile.Profile.ItemStorage.ToString()); });
            displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add("Pokemon Storage", profile.Profile.PokeStorage.ToString()); });

            // get inventory
            var inventory = await client.GetInventory();
            var playerStats = inventory.InventoryDelta.InventoryItems.Select(i => i.InventoryItemData?.PlayerStats).Where(p => p != null).First();
            displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add("Km Walked", playerStats.KmWalked.ToString()); });
            displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add("Level", playerStats.Level.ToString()); });
            displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add("Experience", playerStats.Experience.ToString()); });
            displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add("Next Level XP", playerStats.NextLevelXp.ToString()); });
            displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add("Unique Pokedex Entry", playerStats.UniquePokedexEntries.ToString()); });
        }

        private async void ShowPokemon()
        {
            // setup datagridview
            setDGVColumns(1);

            // get inventory
            var inventory = await client.GetInventory();

            // get pokemons in the inventory
            var pokemons = inventory.InventoryDelta.InventoryItems.Select(i => i.InventoryItemData?.Pokemon)
                           .Where(p => p != null && p?.PokemonId > 0);

            foreach (var pokemon in pokemons)
            {
                try {
                    displayDGV.Invoke((Action)delegate {
                        displayDGV.Rows.Add(
                            (int)pokemon.PokemonId,
                            getPokemonImage(pokemon.PokemonId),
                            pokemon.PokemonId.ToString(),
                            pokemon.Cp,
                            pokemon.HeightM,
                            pokemon.WeightKg,
                            pokemon.Nickname,
                            pokemon.Stamina,
                            pokemon.StaminaMax);
                    });
                }
                catch
                { }
            }
        }

        private async void ShowInventory()
        {
            // setup datagridview
            setDGVColumns(2);

            // get inventory
            var inventory = await client.GetInventory();

            // applied items
            foreach (var item in inventory.InventoryDelta.InventoryItems.Select(i => i.InventoryItemData?.AppliedItems).Where(p => p != null))
                displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add(item.Item.ItemType.ToString() + " (applied)", 1); });

            // items in inventory
            foreach (var item in inventory.InventoryDelta.InventoryItems.Select(i => i.InventoryItemData?.Item).Where(p => p != null))
                displayDGV.Invoke((Action)delegate { displayDGV.Rows.Add(item.Item_.ToString(), item.Count); });
        }

        #endregion

        #region SELECTOR EVENTS

        private void btnProfile_Click(object sender, EventArgs e)
        {
            // GUI settings
            btnProfile.BackColor = Color.WhiteSmoke;
            btnShowPokemon.BackColor = Color.DimGray;
            //btnShowItems.BackColor = Color.DimGray;
            btnLog.BackColor = Color.DimGray;

            txtLog.Visible = false;

            Task.Run(() => ShowProfile());
        }

        private void btnShowPokemon_Click(object sender, EventArgs e)
        {
            // GUI settings
            btnProfile.BackColor = Color.DimGray;
            btnShowPokemon.BackColor = Color.WhiteSmoke;
            //btnShowItems.BackColor = Color.DimGray;
            btnLog.BackColor = Color.DimGray;

            txtLog.Visible = false;

            Task.Run(() => ShowPokemon());
        }

        private void btnShowItems_Click(object sender, EventArgs e)
        {
            // GUI settings
            btnProfile.BackColor = Color.DimGray;
            btnShowPokemon.BackColor = Color.DimGray;
            //btnShowItems.BackColor = Color.WhiteSmoke;
            btnLog.BackColor = Color.DimGray;

            txtLog.Visible = false;

            Task.Run(() => ShowInventory());
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            // GUI settings
            btnProfile.BackColor = Color.DimGray;
            btnShowPokemon.BackColor = Color.DimGray;
            btnShowItems.BackColor = Color.DimGray;
            btnLog.BackColor = Color.WhiteSmoke;

            txtLog.Visible = true;
        }
        #endregion
    }
}
