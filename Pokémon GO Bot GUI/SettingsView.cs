﻿using PokemonGo.RocketAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PokemonGo.RocketAPI.Enums;
using System.Runtime.InteropServices;

namespace Pokémon_GO_Bot_GUI
{
    public partial class SettingsView : Form
    {
        public SettingsView()
        {
            InitializeComponent();
        }

        #region MAKE FORM MOVABLE
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void SettingsView_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        #endregion

       // private static ISettings ClientSettings = new Settings();

        private void SettingsView_Load(object sender, EventArgs e)
        {
            // load datagridview
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.HeaderText = "Key";
            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            settingsDGV.Invoke((Action)delegate { settingsDGV.Columns.Add(column); });

            DataGridViewCheckBoxColumn column2 = new DataGridViewCheckBoxColumn();
            column2.HeaderText = "(cp) Do Not Transfer";
            column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            settingsDGV.Invoke((Action)delegate { settingsDGV.Columns.Add(column2); });

            column2 = new DataGridViewCheckBoxColumn();
            column2.HeaderText = "(leaveStrongest) Unwanted";
            column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            settingsDGV.Invoke((Action)delegate { settingsDGV.Columns.Add(column2); });

            foreach (AllEnum.PokemonId pokemon in Enum.GetValues(typeof(AllEnum.PokemonId)))
            {
                if ((int)pokemon != 0)
                {
                    settingsDGV.Invoke((Action)delegate {
                        settingsDGV.Rows.Add(pokemon.ToString(), 
                            ((from q in MainView.ClientSettings.DoNotTransfer where q == pokemon select q).Count() > 0),
                            ((from q in MainView.ClientSettings.UnwantedPokemon where q == pokemon select q).Count() > 0));
                    });
                }
            }

            txtUsername.Text = MainView.ClientSettings.PtcUsername;
            txtPassword.Text = MainView.ClientSettings.PtcPassword;
            txtLat.Text = MainView.ClientSettings.DefaultLatitude.ToString();
            txtLon.Text = MainView.ClientSettings.DefaultLongitude.ToString();
            cbAuthType.Text = MainView.ClientSettings.AuthType.ToString();
            cbTransferType.Text = MainView.ClientSettings.TransferType;
            numTransferCPThreshold.Value = MainView.ClientSettings.TransferCPThreshold;
            chkEvolveAllGivenPokemon.Checked = MainView.ClientSettings.EvolveAllGivenPokemons;
            txtGoogleToken.Text = MainView.ClientSettings.GoogleRefreshToken;
        }

        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            MainView.ClientSettings.PtcUsername = txtUsername.Text.Trim();
            MainView.ClientSettings.PtcPassword = txtPassword.Text.Trim();
            MainView.ClientSettings.DefaultLatitude = Convert.ToDouble(txtLat.Text.Trim());
            MainView.ClientSettings.DefaultLongitude = Convert.ToDouble(txtLon.Text.Trim());
            MainView.ClientSettings.AuthType = (AuthType)Enum.Parse(typeof(AuthType), cbAuthType.Text);
            MainView.ClientSettings.TransferType = cbTransferType.Text;
            MainView.ClientSettings.TransferCPThreshold = Convert.ToInt32(numTransferCPThreshold.Value);
            MainView.ClientSettings.EvolveAllGivenPokemons = chkEvolveAllGivenPokemon.Checked;
            MainView.ClientSettings.GoogleRefreshToken = txtGoogleToken.Text.Trim();

            // save pokemons lists
            List<AllEnum.PokemonId> DoNotTransfer = new List<AllEnum.PokemonId>();
            List<AllEnum.PokemonId> UnwantedPokemon = new List<AllEnum.PokemonId>();

            foreach (DataGridViewRow row in settingsDGV.Rows)
            {
               if ((bool)row.Cells[1].Value == true)
                    DoNotTransfer.Add((AllEnum.PokemonId)(row.Index + 1));

                if ((bool)row.Cells[2].Value == true)
                    UnwantedPokemon.Add((AllEnum.PokemonId)(row.Index + 1));
            }

            MainView.ClientSettings.DoNotTransfer = DoNotTransfer;
            MainView.ClientSettings.UnwantedPokemon = UnwantedPokemon;

            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void settingsDGV_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 0) e.Cancel = true;
        }
    }
}
