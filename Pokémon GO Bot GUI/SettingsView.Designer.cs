﻿namespace Pokémon_GO_Bot_GUI
{
    partial class SettingsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblLatitude = new System.Windows.Forms.Label();
            this.txtLat = new System.Windows.Forms.TextBox();
            this.lblLongitude = new System.Windows.Forms.Label();
            this.txtLon = new System.Windows.Forms.TextBox();
            this.panelTop = new System.Windows.Forms.Panel();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.lblAuthType = new System.Windows.Forms.Label();
            this.cbAuthType = new System.Windows.Forms.ComboBox();
            this.cbTransferType = new System.Windows.Forms.ComboBox();
            this.lblTransferType = new System.Windows.Forms.Label();
            this.lblTransferCPThreshold = new System.Windows.Forms.Label();
            this.numTransferCPThreshold = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.lblPokestopDelay = new System.Windows.Forms.Label();
            this.lblEvolveAllGivenPokemon = new System.Windows.Forms.Label();
            this.chkEvolveAllGivenPokemon = new System.Windows.Forms.CheckBox();
            this.panelBG = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.settingsDGV = new System.Windows.Forms.DataGridView();
            this.lblGoogleToken = new System.Windows.Forms.Label();
            this.txtGoogleToken = new System.Windows.Forms.TextBox();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTransferCPThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.panelBG.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(205, 18);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(116, 21);
            this.txtUsername.TabIndex = 0;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(205, 45);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(116, 21);
            this.txtPassword.TabIndex = 1;
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Location = new System.Drawing.Point(109, 326);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(109, 43);
            this.btnSaveSettings.TabIndex = 9;
            this.btnSaveSettings.Text = "SAVE";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.Gainsboro;
            this.btnClose.Location = new System.Drawing.Point(617, 1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(28, 24);
            this.btnClose.TabIndex = 100;
            this.btnClose.Text = "X";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.ForeColor = System.Drawing.Color.Black;
            this.lblUsername.Location = new System.Drawing.Point(15, 21);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(66, 15);
            this.lblUsername.TabIndex = 101;
            this.lblUsername.Text = "Username";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.ForeColor = System.Drawing.Color.Black;
            this.lblPassword.Location = new System.Drawing.Point(15, 48);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(65, 15);
            this.lblPassword.TabIndex = 102;
            this.lblPassword.Text = "Password";
            // 
            // lblLatitude
            // 
            this.lblLatitude.AutoSize = true;
            this.lblLatitude.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLatitude.ForeColor = System.Drawing.Color.Black;
            this.lblLatitude.Location = new System.Drawing.Point(15, 75);
            this.lblLatitude.Name = "lblLatitude";
            this.lblLatitude.Size = new System.Drawing.Size(53, 15);
            this.lblLatitude.TabIndex = 103;
            this.lblLatitude.Text = "Latitude";
            // 
            // txtLat
            // 
            this.txtLat.Location = new System.Drawing.Point(205, 72);
            this.txtLat.Name = "txtLat";
            this.txtLat.Size = new System.Drawing.Size(116, 21);
            this.txtLat.TabIndex = 2;
            // 
            // lblLongitude
            // 
            this.lblLongitude.AutoSize = true;
            this.lblLongitude.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLongitude.ForeColor = System.Drawing.Color.Black;
            this.lblLongitude.Location = new System.Drawing.Point(15, 102);
            this.lblLongitude.Name = "lblLongitude";
            this.lblLongitude.Size = new System.Drawing.Size(63, 15);
            this.lblLongitude.TabIndex = 104;
            this.lblLongitude.Text = "Longitude";
            // 
            // txtLon
            // 
            this.txtLon.Location = new System.Drawing.Point(205, 99);
            this.txtLon.Name = "txtLon";
            this.txtLon.Size = new System.Drawing.Size(116, 21);
            this.txtLon.TabIndex = 3;
            // 
            // panelTop
            // 
            this.panelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTop.BackColor = System.Drawing.Color.Gainsboro;
            this.panelTop.Controls.Add(this.btnClose);
            this.panelTop.Location = new System.Drawing.Point(-11, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(654, 27);
            this.panelTop.TabIndex = 12;
            this.panelTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SettingsView_MouseDown);
            // 
            // panelBottom
            // 
            this.panelBottom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panelBottom.Location = new System.Drawing.Point(-3, 408);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(646, 27);
            this.panelBottom.TabIndex = 13;
            // 
            // lblAuthType
            // 
            this.lblAuthType.AutoSize = true;
            this.lblAuthType.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAuthType.ForeColor = System.Drawing.Color.Black;
            this.lblAuthType.Location = new System.Drawing.Point(15, 129);
            this.lblAuthType.Name = "lblAuthType";
            this.lblAuthType.Size = new System.Drawing.Size(59, 15);
            this.lblAuthType.TabIndex = 105;
            this.lblAuthType.Text = "AuthType";
            // 
            // cbAuthType
            // 
            this.cbAuthType.FormattingEnabled = true;
            this.cbAuthType.Items.AddRange(new object[] {
            "Google",
            "Ptc"});
            this.cbAuthType.Location = new System.Drawing.Point(205, 126);
            this.cbAuthType.Name = "cbAuthType";
            this.cbAuthType.Size = new System.Drawing.Size(116, 23);
            this.cbAuthType.TabIndex = 4;
            // 
            // cbTransferType
            // 
            this.cbTransferType.FormattingEnabled = true;
            this.cbTransferType.Items.AddRange(new object[] {
            "none",
            "cp",
            "leaveStrongest",
            "duplicate",
            "all"});
            this.cbTransferType.Location = new System.Drawing.Point(205, 155);
            this.cbTransferType.Name = "cbTransferType";
            this.cbTransferType.Size = new System.Drawing.Size(116, 23);
            this.cbTransferType.TabIndex = 5;
            // 
            // lblTransferType
            // 
            this.lblTransferType.AutoSize = true;
            this.lblTransferType.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransferType.ForeColor = System.Drawing.Color.Black;
            this.lblTransferType.Location = new System.Drawing.Point(15, 158);
            this.lblTransferType.Name = "lblTransferType";
            this.lblTransferType.Size = new System.Drawing.Size(84, 15);
            this.lblTransferType.TabIndex = 106;
            this.lblTransferType.Text = "Transfer Type";
            // 
            // lblTransferCPThreshold
            // 
            this.lblTransferCPThreshold.AutoSize = true;
            this.lblTransferCPThreshold.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransferCPThreshold.ForeColor = System.Drawing.Color.Black;
            this.lblTransferCPThreshold.Location = new System.Drawing.Point(15, 187);
            this.lblTransferCPThreshold.Name = "lblTransferCPThreshold";
            this.lblTransferCPThreshold.Size = new System.Drawing.Size(134, 15);
            this.lblTransferCPThreshold.TabIndex = 107;
            this.lblTransferCPThreshold.Text = "Transfer CP Threshold";
            // 
            // numTransferCPThreshold
            // 
            this.numTransferCPThreshold.Location = new System.Drawing.Point(205, 185);
            this.numTransferCPThreshold.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numTransferCPThreshold.Name = "numTransferCPThreshold";
            this.numTransferCPThreshold.Size = new System.Drawing.Size(116, 21);
            this.numTransferCPThreshold.TabIndex = 6;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Enabled = false;
            this.numericUpDown1.Location = new System.Drawing.Point(205, 212);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(116, 21);
            this.numericUpDown1.TabIndex = 7;
            // 
            // lblPokestopDelay
            // 
            this.lblPokestopDelay.AutoSize = true;
            this.lblPokestopDelay.Enabled = false;
            this.lblPokestopDelay.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPokestopDelay.ForeColor = System.Drawing.Color.Black;
            this.lblPokestopDelay.Location = new System.Drawing.Point(15, 214);
            this.lblPokestopDelay.Name = "lblPokestopDelay";
            this.lblPokestopDelay.Size = new System.Drawing.Size(95, 15);
            this.lblPokestopDelay.TabIndex = 108;
            this.lblPokestopDelay.Text = "Pokestop Delay";
            // 
            // lblEvolveAllGivenPokemon
            // 
            this.lblEvolveAllGivenPokemon.AutoSize = true;
            this.lblEvolveAllGivenPokemon.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEvolveAllGivenPokemon.ForeColor = System.Drawing.Color.Black;
            this.lblEvolveAllGivenPokemon.Location = new System.Drawing.Point(15, 243);
            this.lblEvolveAllGivenPokemon.Name = "lblEvolveAllGivenPokemon";
            this.lblEvolveAllGivenPokemon.Size = new System.Drawing.Size(151, 15);
            this.lblEvolveAllGivenPokemon.TabIndex = 109;
            this.lblEvolveAllGivenPokemon.Text = "Evolve All Given Pokemon";
            // 
            // chkEvolveAllGivenPokemon
            // 
            this.chkEvolveAllGivenPokemon.AutoSize = true;
            this.chkEvolveAllGivenPokemon.Location = new System.Drawing.Point(249, 244);
            this.chkEvolveAllGivenPokemon.Name = "chkEvolveAllGivenPokemon";
            this.chkEvolveAllGivenPokemon.Size = new System.Drawing.Size(15, 14);
            this.chkEvolveAllGivenPokemon.TabIndex = 8;
            this.chkEvolveAllGivenPokemon.UseVisualStyleBackColor = true;
            // 
            // panelBG
            // 
            this.panelBG.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBG.BackColor = System.Drawing.Color.DimGray;
            this.panelBG.Controls.Add(this.panelBottom);
            this.panelBG.Controls.Add(this.panelTop);
            this.panelBG.Controls.Add(this.panel1);
            this.panelBG.Location = new System.Drawing.Point(0, 0);
            this.panelBG.Name = "panelBG";
            this.panelBG.Size = new System.Drawing.Size(637, 435);
            this.panelBG.TabIndex = 24;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.settingsDGV);
            this.panel1.Controls.Add(this.lblGoogleToken);
            this.panel1.Controls.Add(this.txtGoogleToken);
            this.panel1.Controls.Add(this.lblUsername);
            this.panel1.Controls.Add(this.lblAuthType);
            this.panel1.Controls.Add(this.cbAuthType);
            this.panel1.Controls.Add(this.lblLongitude);
            this.panel1.Controls.Add(this.chkEvolveAllGivenPokemon);
            this.panel1.Controls.Add(this.lblTransferType);
            this.panel1.Controls.Add(this.txtUsername);
            this.panel1.Controls.Add(this.txtLon);
            this.panel1.Controls.Add(this.lblEvolveAllGivenPokemon);
            this.panel1.Controls.Add(this.cbTransferType);
            this.panel1.Controls.Add(this.txtPassword);
            this.panel1.Controls.Add(this.lblLatitude);
            this.panel1.Controls.Add(this.numericUpDown1);
            this.panel1.Controls.Add(this.lblTransferCPThreshold);
            this.panel1.Controls.Add(this.btnSaveSettings);
            this.panel1.Controls.Add(this.txtLat);
            this.panel1.Controls.Add(this.lblPokestopDelay);
            this.panel1.Controls.Add(this.numTransferCPThreshold);
            this.panel1.Controls.Add(this.lblPassword);
            this.panel1.Location = new System.Drawing.Point(3, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(627, 389);
            this.panel1.TabIndex = 24;
            // 
            // settingsDGV
            // 
            this.settingsDGV.AllowUserToAddRows = false;
            this.settingsDGV.AllowUserToDeleteRows = false;
            this.settingsDGV.AllowUserToResizeRows = false;
            this.settingsDGV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.settingsDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.settingsDGV.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.settingsDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.settingsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.settingsDGV.Cursor = System.Windows.Forms.Cursors.Default;
            this.settingsDGV.Location = new System.Drawing.Point(328, 18);
            this.settingsDGV.Name = "settingsDGV";
            this.settingsDGV.RowHeadersVisible = false;
            this.settingsDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.settingsDGV.Size = new System.Drawing.Size(283, 351);
            this.settingsDGV.TabIndex = 112;
            this.settingsDGV.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.settingsDGV_CellBeginEdit);
            // 
            // lblGoogleToken
            // 
            this.lblGoogleToken.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblGoogleToken.AutoSize = true;
            this.lblGoogleToken.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoogleToken.ForeColor = System.Drawing.Color.Black;
            this.lblGoogleToken.Location = new System.Drawing.Point(124, 269);
            this.lblGoogleToken.Name = "lblGoogleToken";
            this.lblGoogleToken.Size = new System.Drawing.Size(83, 15);
            this.lblGoogleToken.TabIndex = 111;
            this.lblGoogleToken.Text = "Google Token";
            // 
            // txtGoogleToken
            // 
            this.txtGoogleToken.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtGoogleToken.Location = new System.Drawing.Point(14, 291);
            this.txtGoogleToken.Name = "txtGoogleToken";
            this.txtGoogleToken.Size = new System.Drawing.Size(303, 21);
            this.txtGoogleToken.TabIndex = 110;
            // 
            // SettingsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(633, 434);
            this.Controls.Add(this.panelBG);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SettingsView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SettingsView";
            this.Load += new System.EventHandler(this.SettingsView_Load);
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numTransferCPThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.panelBG.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnSaveSettings;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblLatitude;
        private System.Windows.Forms.TextBox txtLat;
        private System.Windows.Forms.Label lblLongitude;
        private System.Windows.Forms.TextBox txtLon;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Label lblAuthType;
        private System.Windows.Forms.ComboBox cbAuthType;
        private System.Windows.Forms.ComboBox cbTransferType;
        private System.Windows.Forms.Label lblTransferType;
        private System.Windows.Forms.Label lblTransferCPThreshold;
        private System.Windows.Forms.NumericUpDown numTransferCPThreshold;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label lblPokestopDelay;
        private System.Windows.Forms.Label lblEvolveAllGivenPokemon;
        private System.Windows.Forms.CheckBox chkEvolveAllGivenPokemon;
        private System.Windows.Forms.Panel panelBG;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtGoogleToken;
        private System.Windows.Forms.Label lblGoogleToken;
        private System.Windows.Forms.DataGridView settingsDGV;
    }
}