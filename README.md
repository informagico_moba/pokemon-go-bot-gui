# Pokemon-Go-BOT-GUI

![LOG](https://bitbucket.org/bwrkr/pokemon-go-bot-gui/raw/master/log.jpg)


### A Pokemon GO Bot in C# (with GUI)


## TO DO
* Disable pokemon catching when there's no more space for pokemons
* Make a better evolving algorithm (i.e.: evolving just pokemon that are not in pokedex/inventory)
* Fix random crashes when changing section
* Fix wrong message when evolving - i.e.: 
    * [19:47:01] Evolving Pidgeotto...successfull for 500 xp!
    * [19:47:01] Evolving Pidgeotto...failed due to pokemon missing!
* Implement use of RazzBerries and auto-recycle in the GUI


## DONE
* [FIXED] Fix a bug fetching pokestop object:
    * [22:06:11] PokeStop XP: 50, Gems: 0, Eggs: { "id": "1406230206xxxxxxxxxx", "isEgg": true, "capturedCellId": "xxxxxxxxxx409096704", "creationTimeMs": "146xxxxxxxxxx" } Items: 3 x ItemPokeBall
* Automate use of RazzBerries and auto-recycle items
* Implement Inventory section.
* Make white/blacklist of pokemon to transfer editable directly from the GUI


## Features
* PTC / Google Login
* Get Map Objects and Inventory
* Search for Gyms / Pokéstops / Spawns
* Farm Pokéstops
* Farm all Pokémon in the neighbourhood
* Evolve Pokémon
* Transfer Pokémon
* User friendly GUI with a lot of informations
* Auto-Recycle uneeded items
* Automatic use of RazzBerries


## Precompiled binaries (just download and launch)

(v1.0 - 85f0516) https://mega.nz/#!yYUDELJR!GyQEinUKdk3loF4AMmDDOFBBpDGceIuDaymPhMIeenU
(v1.1 - d69f323) https://mega.nz/#!iMER2IDL!y77FL9AUD3RHNHewkM49lrWpj7_XBR0yln2zHLbUvsc


## Getting Started (from sources)

Just download sources, open them in VS and hit Build and Run (CTRL+F5).
Once the application is started you must edit settings in Edit>Settings menu.

![SETTINGS](https://bitbucket.org/bwrkr/pokemon-go-bot-gui/raw/master/settings.jpg)

To start the bot just click on File>Login (Bot is not started yey, but you can see your Inventory and Profile) and then File>Start actually starts the Bot.

![PROFILE](https://bitbucket.org/bwrkr/pokemon-go-bot-gui/raw/master/profile.jpg)

![POKEMONS](https://bitbucket.org/bwrkr/pokemon-go-bot-gui/raw/master/pokemons.jpg)

In LOG section you can see a CLI Style log, on the right there's an interactive map that shows all current session's events.

If you double-click on the map, in any location, it will ask you if set that as Bot's location.


## Transfer Types

The most popular option is probably the duplicate type that removes all duplicates and leaves you one pokemon of each type with the highest CP.

* none - disables transferring
* cp - transfers all pokemon below the CP threshold, EXCEPT for those white-list (can be modified via GUI)
* leaveStrongest - transfers all but the highest CP pokemon of each type in an hardcoded list (those that aren't specified are untouched) (can be modified via GUI)
* duplicate - same as above but for all pokemon (no need to specify type)
* all - transfers all pokemon

TransferCPThreshold let the bot transfer pokemon with CP less than this value if cp transfer type is selected.


# Credits:
Many thanks to Ferox + Neer + nonm for the API.
Thanks also to AHAAAAAAA devs for the assets.
Thanks to Sen66 for automatic use of RazzBerry and Recycle Inventory.

* https://github.com/DetectiveSquirrel/Pokemon-Go-Rocket-API
* https://github.com/AHAAAAAAA/PokemonGo-Map.git
* https://github.com/Sen66/PokemonGo-Bot