﻿#region

using PokemonGo.RocketAPI.Enums;
using System.Collections.Generic;

#endregion

namespace PokemonGo.RocketAPI
{
    public interface ISettings
    {
        AuthType AuthType { get; set; }
        double DefaultLatitude { get; set; }
        double DefaultLongitude { get; set; }
        string GoogleRefreshToken { get; set; }
        string PtcPassword { get; set; }
        string PtcUsername { get; set; }
        bool EvolveAllGivenPokemons { get; set; }
        string TransferType { get; set; }
        int TransferCPThreshold { get; set; }
        bool Recycler { get; set; }
        ICollection<KeyValuePair<AllEnum.ItemId, int>> ItemRecycleFilter { get; }
        int RecycleItemsInterval { get; set; }
        string RazzBerryMode { get; set; }
        double RazzBerrySetting { get; set; }
        List<AllEnum.PokemonId> UnwantedPokemon { get; set; }
        List<AllEnum.PokemonId> DoNotTransfer { get; set; }
    }
}